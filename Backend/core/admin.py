from django.contrib import admin
from .models import (
    Administradores, 
    Clinicas, 
    AdminConsultorios, 
    Medicos, 
    Empleados, 
    Pacientes, 
    Historiales, 
    Citas, 
    Pagos, 
    Tratamientos, 
    CitasTratamientos, 
    Almacenes, 
    Sensores, 
    Materiales, 
    StocksMateriales,
    User
)
admin.site.register(User)

@admin.register(Administradores)
class AdministradoresAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apPat', 'apMat')
    search_fields = ('nombre', 'apPat', 'apMat')

@admin.register(Clinicas)
class ClinicasAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'direccion')
    search_fields = ('nombre', 'direccion')

@admin.register(AdminConsultorios)
class AdminConsultoriosAdmin(admin.ModelAdmin):
    list_display = ('administrador', 'consultorio')
    search_fields = ('administrador__nombre', 'consultorio__nombre')

@admin.register(Empleados)
class EmpleadosAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apPat', 'apMat', 'correo', 'telefono', 'horario', 'estado')
    search_fields = ('nombre', 'apPat', 'apMat', 'correo')

@admin.register(Medicos)
class MedicosAdmin(admin.ModelAdmin):
    list_display = ('cedula', 'especialidad', 'clinica', 'empleado')
    search_fields = ('cedula', 'especialidad', 'clinica__nombre', 'empleado__nombre')

@admin.register(Pacientes)
class PacientesAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apPat', 'apMat', 'fechaNacimiento', 'direccion', 'telefono', 'correo')
    search_fields = ('nombre', 'apPat', 'apMat', 'correo')

@admin.register(Historiales)
class HistorialesAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'paciente')
    search_fields = ('paciente__nombre', 'paciente__apPat', 'paciente__apMat')

@admin.register(Citas)
class CitasAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'hora', 'tipo', 'estado', 'paciente', 'empleado')
    search_fields = ('paciente__nombre', 'empleado__nombre')

@admin.register(Pagos)
class PagosAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'monto', 'metodoPago', 'cita')
    search_fields = ('cita__paciente__nombre', 'metodoPago')

@admin.register(Tratamientos)
class TratamientosAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion', 'duracion', 'costo', 'categoria')
    search_fields = ('nombre', 'categoria')

@admin.register(CitasTratamientos)
class CitasTratamientosAdmin(admin.ModelAdmin):
    list_display = ('cita', 'tratamiento')
    search_fields = ('cita__paciente__nombre', 'tratamiento__nombre')

@admin.register(Almacenes)
class AlmacenesAdmin(admin.ModelAdmin):
    list_display = ('clinica',)
    search_fields = ('clinica__nombre',)

@admin.register(Sensores)
class SensoresAdmin(admin.ModelAdmin):
    list_display = ('tipo', 'ubicacion', 'nombre', 'fechaInstalacion', 'estado', 'almacen')
    search_fields = ('tipo', 'ubicacion', 'nombre')

@admin.register(Materiales)
class MaterialesAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion', 'almacen')
    search_fields = ('nombre', 'almacen__clinica__nombre')

@admin.register(StocksMateriales)
class StocksMaterialesAdmin(admin.ModelAdmin):
    list_display = ('material', 'tratamiento', 'stock')
    search_fields = ('material__nombre', 'tratamiento__nombre')
