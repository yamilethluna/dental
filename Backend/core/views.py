from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

from .models import (
    Administradores,
    Clinicas,
    AdminConsultorios,
    Empleados,
    Medicos,
    Pacientes,
    Historiales,
    Citas,
    Pagos,
    Tratamientos,
    CitasTratamientos,
    Almacenes,
    Sensores,
    Materiales,
    StocksMateriales,
)
from .serializers import (
    AdministradoresSerializer,
    ClinicasSerializer,
    AdminConsultoriosSerializer,
    EmpleadosSerializer,
    MedicosSerializer,
    PacientesSerializer,
    HistorialesSerializer,
    CitasSerializer,
    PagosSerializer,
    TratamientosSerializer,
    CitasTratamientosSerializer,
    AlmacenesSerializer,
    SensoresSerializer,
    MaterialesSerializer,
    StocksMaterialesSerializer,
)
# users/views.py
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from .serializers import LoginSerializer, UserSerializer

class LoginView(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user': UserSerializer(user).data
        })

# Define tus viewsets para cada modelo
class AdministradoresViewSet(viewsets.ModelViewSet):
    queryset = Administradores.objects.all()
    serializer_class = AdministradoresSerializer

class ClinicasViewSet(viewsets.ModelViewSet):
    queryset = Clinicas.objects.all()
    serializer_class = ClinicasSerializer

class AdminConsultoriosViewSet(viewsets.ModelViewSet):
    queryset = AdminConsultorios.objects.all()
    serializer_class = AdminConsultoriosSerializer

class EmpleadosViewSet(viewsets.ModelViewSet):
    queryset = Empleados.objects.all()
    serializer_class = EmpleadosSerializer

class MedicosViewSet(viewsets.ModelViewSet):
    queryset = Medicos.objects.all()
    serializer_class = MedicosSerializer

class PacientesViewSet(viewsets.ModelViewSet):
    queryset = Pacientes.objects.all()
    serializer_class = PacientesSerializer

class HistorialesViewSet(viewsets.ModelViewSet):
    queryset = Historiales.objects.all()
    serializer_class = HistorialesSerializer

class CitasViewSet(viewsets.ModelViewSet):
    queryset = Citas.objects.all()
    serializer_class = CitasSerializer

class PagosViewSet(viewsets.ModelViewSet):
    queryset = Pagos.objects.all()
    serializer_class = PagosSerializer

class TratamientosViewSet(viewsets.ModelViewSet):
    queryset = Tratamientos.objects.all()
    serializer_class = TratamientosSerializer

class CitasTratamientosViewSet(viewsets.ModelViewSet):
    queryset = CitasTratamientos.objects.all()
    serializer_class = CitasTratamientosSerializer

class AlmacenesViewSet(viewsets.ModelViewSet):
    queryset = Almacenes.objects.all()
    serializer_class = AlmacenesSerializer

class SensoresViewSet(viewsets.ModelViewSet):
    queryset = Sensores.objects.all()
    serializer_class = SensoresSerializer

class MaterialesViewSet(viewsets.ModelViewSet):
    queryset = Materiales.objects.all()
    serializer_class = MaterialesSerializer

class StocksMaterialesViewSet(viewsets.ModelViewSet):
    queryset = StocksMateriales.objects.all()
    serializer_class = StocksMaterialesSerializer

