// screens/LoginScreen.js
import React, { useState } from 'react';
import { View, Text, TextInput, Button, Alert } from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native'; // Importa useNavigation
import { API_URL } from "../config";

const ip = API_URL

export default function LoginScreen() {
    const navigation = useNavigation(); // Obtén la función de navegación

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = () => {
        axios.post('http://' + ip + ':8000/api/core/login/', { email, password })
            .then(response => {
                Alert.alert('Inicio de sesión exitoso', `Bienvenido ${response.data.user.email}`);
                navigation.navigate('CreateEmpleados'); // Navega a la pantalla CreateEmpleadosScreen
            })
            .catch(error => {
                Alert.alert('Error', 'Credenciales incorrectas');
            });
    };

    return (
        <View>
            <Text>Correo electrónico:</Text>
            <TextInput value={email} onChangeText={setEmail} keyboardType="email-address" />
            <Text>Contraseña:</Text>
            <TextInput value={password} onChangeText={setPassword} secureTextEntry />
            <Button title="Iniciar sesión" onPress={handleLogin} />
        </View>
    );
}
