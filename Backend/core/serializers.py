from rest_framework import serializers
from django.contrib.auth import authenticate
from .models import (
    Administradores,
    Clinicas,
    AdminConsultorios,
    Empleados,
    Medicos,
    Pacientes,
    Historiales,
    Citas,
    Pagos,
    Tratamientos,
    CitasTratamientos,
    Almacenes,
    Sensores,
    Materiales,
    StocksMateriales,
    User
)

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(write_only=True)

    def validate(self, data):
        email = data.get('email')
        password = data.get('password')
        user = authenticate(email=email, password=password)
        if not user:
            raise serializers.ValidationError('Credenciales incorrectas')
        return user

class AdministradoresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Administradores
        fields = '__all__'

class ClinicasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clinicas
        fields = '__all__'

class AdminConsultoriosSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminConsultorios
        fields = '__all__'

class EmpleadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empleados
        fields = '__all__'

class MedicosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medicos
        fields = '__all__'

class PacientesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pacientes
        fields = '__all__'

class HistorialesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Historiales
        fields = '__all__'

class CitasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Citas
        fields = '__all__'

class PagosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pagos
        fields = '__all__'

class TratamientosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tratamientos
        fields = '__all__'

class CitasTratamientosSerializer(serializers.ModelSerializer):
    class Meta:
        model = CitasTratamientos
        fields = '__all__'

class AlmacenesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Almacenes
        fields = '__all__'

class SensoresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensores
        fields = '__all__'

class MaterialesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Materiales
        fields = '__all__'

class StocksMaterialesSerializer(serializers.ModelSerializer):
    class Meta:
        model = StocksMateriales
        fields = '__all__'
