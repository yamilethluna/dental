import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const EmpleadoScreen = ({ route }) => {
  const { userInfo } = route.params;

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Bienvenido Empleado</Text>
      <Text style={styles.text}>Nombre: {userInfo.nombre}</Text>
      <Text style={styles.text}>Email: {userInfo.email}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  text: {
    fontSize: 20,
    marginBottom: 10,
  },
});

export default EmpleadoScreen;
