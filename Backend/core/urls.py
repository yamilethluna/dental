from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import (
    AdministradoresViewSet,
    ClinicasViewSet,
    AdminConsultoriosViewSet,
    EmpleadosViewSet,
    MedicosViewSet,
    PacientesViewSet,
    HistorialesViewSet,
    CitasViewSet,
    PagosViewSet,
    TratamientosViewSet,
    CitasTratamientosViewSet,
    AlmacenesViewSet,
    SensoresViewSet,
    MaterialesViewSet,
    StocksMaterialesViewSet,
)

router = DefaultRouter()
router.register(r'administradores', AdministradoresViewSet)
router.register(r'clinicas', ClinicasViewSet)
router.register(r'admin-consultorios', AdminConsultoriosViewSet)
router.register(r'empleados', EmpleadosViewSet)
router.register(r'medicos', MedicosViewSet)
router.register(r'pacientes', PacientesViewSet)
router.register(r'historiales', HistorialesViewSet)
router.register(r'citas', CitasViewSet)
router.register(r'pagos', PagosViewSet)
router.register(r'tratamientos', TratamientosViewSet)
router.register(r'citas-tratamientos', CitasTratamientosViewSet)
router.register(r'almacenes', AlmacenesViewSet)
router.register(r'sensores', SensoresViewSet)
router.register(r'materiales', MaterialesViewSet)
router.register(r'stocks-materiales', StocksMaterialesViewSet)

from .views import LoginView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('', include(router.urls)),
]
