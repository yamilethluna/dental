import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet } from 'react-native';
import axios from 'axios';
import { API_URL } from "../../config";

const ip = API_URL

const CreateEmpleadosScreen = () => {
  const [nombre, setNombre] = useState('');
  const [apPat, setApPat] = useState('');
  const [apMat, setApMat] = useState('');
  const [horario, setHorario] = useState('');
  const [telefono, setTelefono] = useState('');
  const [correo, setCorreo] = useState('');
  const [contrasenia, setContrasenia] = useState('');


  const handleSubmit = async () => {
    try {
      const response = await axios.post('http://' + ip + ':8000/api/empleados/', {
        nombre,
        apPat,
        apMat,
        horario,
        telefono,
        correo,
        contrasenia,
        estado: 'activo',

      });
      console.log(response.data);
      // Mostrar mensaje de éxito o navegar a otra pantalla
    } catch (error) {
        console.error(error.response); // Imprime toda la respuesta de error
        console.error(error.response.data); // Imprime solo el cuerpo de la respuesta de error
        // Mostrar mensaje de error al usuario
      }
      
  };

  return (
    <>
      <View style={styles.container}>
        <TextInput
          placeholder="Nombre"
          value={nombre}
          onChangeText={setNombre}
          style={styles.input}
        />
        <TextInput
          placeholder="Apellido Paterno"
          value={apPat}
          onChangeText={setApPat}
          style={styles.input}
        />
        <TextInput
          placeholder="Apellido Materno"
          value={apMat}
          onChangeText={setApMat}
          style={styles.input}
        />
        <TextInput
          placeholder="Horario"
          value={horario}
          onChangeText={setHorario}
          style={styles.input}
        />
        <TextInput
          placeholder="Teléfono"
          value={telefono}
          onChangeText={setTelefono}
          style={styles.input}
        />
        <TextInput
          placeholder="Correo electrónico"
          value={correo}
          onChangeText={setCorreo}
          style={styles.input}
        />
        <TextInput
          placeholder="Contrasenia"
          value={contrasenia}
          onChangeText={setContrasenia}
          style={styles.input}
        />
        <Button title="Registrar Empleado" onPress={handleSubmit} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  input: {
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    width: '100%',
  },
});

export default CreateEmpleadosScreen;
